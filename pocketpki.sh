#!/bin/sh
#
#
# PocketPKI
#   a small script to create your own, 'pocket-sized PKI', i.e. 
#   a bunch of CAs to create test certificates for several scenarios.
#   Works as a single shell script, wrapping openssl command line.
#   Note that this must only be used for testing several different
#   types of certificates, chains, etc. Do not use this script to
#   issue production certificates. 
#
#
#   the configs where based on the tutorial: https://pki-tutorial.readthedocs.org
#
#
# Repository:
# https://bitbucket.com/d13/PocketPKI.git
#
# Home:
# https://cryptotactics.com/pocketpki
# @d13sector
# created January, 2014
# v1.0 - RootCA, IntermediateCA, IssuingCA -> ServerCert, ClientCert

######################################################################
##
## Configuration - Update these values for your environment
##
######################################################################
PPKI_PATH=./
# CAs' configurations
PPKI_COUNTRY="EU"
PPKI_ORGANIZATION="Cryptotactics"
PPKI_CN="PocketPKI Root CA"
PPKI_INTERMEDIATE_CN="PocketPKI Intermediate CA"
PPKI_ISSUING_CN="PocketPKI Issuing CA"
PPKI_BASE_URL="http://pki.cryptotactics.com"
PPKI_OCSP_URL="http://ocsp.cryptotactics.com"
# Certs to be issued
# server
PPKI_SERVER_CN="www.adomainname.corp"
# subject alternative name
PPKI_SERVER_SAN="DNS:bdomainname.corp,DNS:www.bdomainname.corp"
# client
PPKI_CLIENT_CN="John Doe"
# Common password used in private keys (NOTE: THIS IS FOR TEST PURPOSES ONLY!!!)
PPKI_PASSWORD=Password01
######################################################################
##
## script starts here
##
export PPKI_COUNTRY \
     PPKI_ORGANIZATION \
     PPKI_CN \
     PPKI_INTERMEDIATE_CN \
     PPKI_BASE_URL \
     PPKI_OCSP_URL \
     PPKI_ISSUING_CN \
     PPKI_SERVER_CN \
     PPKI_SERVER_SAN \
     PPKI_CLIENT_CN \
     PPKI_PASSWORD
# Create Root CA
mkdir -p $PPKI_PATH/ca/root-ca/private ca/root-ca/db crl certs
chmod 700 $PPKI_PATH/ca/root-ca/private
cp /dev/null ca/root-ca/db/root-ca.db
cp /dev/null ca/root-ca/db/root-ca.db.attr
echo 01 > ca/root-ca/db/root-ca.crt.srl
echo 01 > ca/root-ca/db/root-ca.crl.srl
openssl req -new \
    -config etc/root-ca.conf \
    -out ca/root-ca.csr \
    -keyout ca/root-ca/private/root-ca.key
openssl ca -selfsign \
    -config etc/root-ca.conf \
    -in ca/root-ca.csr \
    -out ca/root-ca.crt \
    -extensions root_ca_ext \
    -passin pass:$PPKI_PASSWORD \
    -batch \
    -enddate 310101000000Z
openssl ca -gencrl \
    -config etc/root-ca.conf \
    -passin pass:$PPKI_PASSWORD \
    -out crl/root-ca.crl
######################################################################
mkdir -p ca/intermediate-ca/private ca/intermediate-ca/db crl certs
chmod 700 ca/intermediate-ca/private
cp /dev/null ca/intermediate-ca/db/intermediate-ca.db
cp /dev/null ca/intermediate-ca/db/intermediate-ca.db.attr
echo 01 > ca/intermediate-ca/db/intermediate-ca.crt.srl
echo 01 > ca/intermediate-ca/db/intermediate-ca.crl.srl
openssl req -new \
    -config etc/intermediate-ca.conf \
    -out ca/intermediate-ca.csr \
    -keyout ca/intermediate-ca/private/intermediate-ca.key
openssl ca \
    -config etc/root-ca.conf \
    -in ca/intermediate-ca.csr \
    -out ca/intermediate-ca.crt \
    -extensions intermediate_ca_ext \
    -passin pass:$PPKI_PASSWORD \
    -batch \
    -enddate 310101000000Z
openssl ca -gencrl \
    -config etc/intermediate-ca.conf \
    -passin pass:$PPKI_PASSWORD \
    -out crl/intermediate-ca.crl
cat ca/intermediate-ca.crt ca/root-ca.crt > \
    ca/intermediate-ca-chain.pem
######################################################################
mkdir -p ca/issuing-ca/private ca/issuing-ca/db crl certs
chmod 700 ca/issuing-ca/private
cp /dev/null ca/issuing-ca/db/issuing-ca.db
cp /dev/null ca/issuing-ca/db/issuing-ca.db.attr
echo 01 > ca/issuing-ca/db/issuing-ca.crt.srl
echo 01 > ca/issuing-ca/db/issuing-ca.crl.srl
openssl req -new \
    -config etc/issuing-ca.conf \
    -out ca/issuing-ca.csr \
    -keyout ca/issuing-ca/private/issuing-ca.key
openssl ca \
    -config etc/intermediate-ca.conf \
    -in ca/issuing-ca.csr \
    -out ca/issuing-ca.crt \
    -passin pass:$PPKI_PASSWORD \
    -batch \
    -extensions signing_ca_ext
openssl ca -gencrl \
    -config etc/issuing-ca.conf \
    -passin pass:$PPKI_PASSWORD \
    -out crl/issuing-ca.crl
cat ca/issuing-ca.crt ca/intermediate-ca-chain.pem > \
    ca/issuing-ca-chain.pem
######################################################################
# server certificate ( uses var 'SAN' )
openssl req -new \
    -config etc/server.conf \
    -out certs/mydomainname.com.csr \
    -keyout certs/mydomainname.com.key
openssl ca \
    -config etc/issuing-ca.conf \
    -in certs/mydomainname.com.csr \
    -out certs/mydomainname.com.crt \
    -passin pass:$PPKI_PASSWORD \
    -batch \
    -extensions server_ext
######################################################################
# client certificate
openssl req -new \
    -config etc/client.conf \
    -out certs/client.csr \
    -keyout certs/client.key
openssl ca \
    -config etc/issuing-ca.conf \
    -in certs/client.csr \
    -out certs/client.crt \
    -passin pass:$PPKI_PASSWORD \
    -batch \
    -extensions client_ext

