# PocketPKI
PocketPKI is a small script to create your own, 'pocket-sized PKI', i.e. 
a bunch of CAs to create test certificates for several scenarios.
Works as a single shell script, wrapping openssl command line.
Note that this must only be used for testing several different
types of certificates, chains, etc. Do not use this script to
issue production certificates. 

## Configuration

Update the configuration directly in the script: PocketPKI/pocketpki.sh. Available configuration variables (shown with current default values - update to use your own!):

```
#!bash

PPKI_PATH=./
# CAs' configurations
PPKI_COUNTRY="EU"
PPKI_ORGANIZATION="Cryptotactics"
PPKI_CN="PocketPKI Root CA"
PPKI_INTERMEDIATE_CN="PocketPKI Intermediate CA"
PPKI_ISSUING_CN="PocketPKI Issuing CA"
PPKI_BASE_URL="http://pki.cryptotactics.com"
PPKI_OCSP_URL="http://ocsp.cryptotactics.com"
# Certs to be issued
# server
PPKI_SERVER_CN="www.adomainname.corp"
# subject alternative name
PPKI_SERVER_SAN="DNS:bdomainname.corp,DNS:www.bdomainname.corp"
# client
PPKI_CLIENT_CN="John Doe"
# Common password used in private keys (NOTE: THIS IS FOR TEST PURPOSES ONLY!!!)
PPKI_PASSWORD=Password01

```
